<?php 
    include_once 'bot.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <?php if (isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger">
                <?= $_SESSION["error"]; ?>
            </div>
        <?php } ?>
        <form class="col-md-4" action="bot.php" method="post">
            <div class="form-group">
                <label>Enter URL</label>
                <input type="text" name="url_link" id="url_link" class="form-control">
            </div>

            <div class="form-group">
                <input type="submit" name="action" value="find_url" id="find_url" class="btn btn-success">
            </div>
        </form>

        <form class="col-md-4" action="bot.php" method="post">
            <div class="form-group">
                <label>Search</label>
                <input type="text" name="search" id="search" class="form-control" value="<?php if(isset($_SESSION['search_val'])) { print $_SESSION['search_val']; } ?>">
            </div>

            <div class="form-group">
                <input type="submit" name="action" value="search" id="search_but" class="btn btn-success">
            </div>
        </form>

        <form class="col-md-4">
            <div class="form-group">
                <label>Export</label>
                <input type="text" name="export" id="export" class="form-control">
            </div>

            <div class="form-group">
                <input type="button" name="action" value="export" id="export_but" class="btn btn-success">
            </div>
        </form>
        <?php if (isset($_SESSION['msg']) && isset($_SESSION['res_values'])) { ?>
            <div id="search_res_div" >
                <h2><?= $_SESSION['msg']; ?></h2>
                <?php 
                    $results = $_SESSION['res_values'];
                    foreach ($results as $result) { 
                ?>
                    <div>
                        <a target="blank" href="<?= $result['url']; ?>"><?= $result['url']; ?></a>
                    </div>
                <?php
                    } 
                ?>
            </div>
        <?php } ?>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</html>

<?php session_destroy(); ?>
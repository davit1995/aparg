<?php 
session_start();
header('Content-Type: text/html; charset=utf-8');
class Bot
{
    private $db;

    function __construct() {
        $this->db = mysqli_connect("localhost", "root", "", "aparg");
        mysqli_set_charset($this->db, "utf8");
        $this->db->set_charset("utf8");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            call_user_func(array($this,$_POST['action']));
        }
    }

    function find_url() {
        $url = $_POST['url_link'];

        if (empty($url)) {
            $_SESSION['error'] = 'Please add URL';
            header('Location:index.php');
        } else {
            $info = file_get_contents($url);
            $ifExistUrl = $this->db->query('SELECT * FROM urls WHERE url = "' . $url . '"')->fetch_all(MYSQLI_ASSOC);
            if (!empty($ifExistUrl)) {
                $url_id = $ifExistUrl[0]['id'];

                $this->db->query('UPDATE urls SET content = ' . json_encode($info) . ' WHERE url ="' . $url . '"');
            } else {
                $this->db->query("INSERT INTO urls(url, content) values('" . $url . "', " . json_encode($info) . ")");
                $url_id = $this->db->insert_id;
            }
            

            preg_match_all("|<h[^>]+>(.*)</h[^>]+>|iU",$info,$headings);
            preg_match_all("/<title>(.+)<\/title>/i",$info,$titles);
            preg_match_all('/href\=\"(.*?)\"/si',$info,$links);

            if (!empty($headings[1])) {
                unset($headings[1][count($headings[1]) - 1]);

                foreach ($headings[1] as $heading) {

                    $ifExistHeading = $this->db->query("SELECT * FROM headings WHERE url_id = '" . $url_id . "' and heading = '" . $heading . "'")->fetch_all(MYSQLI_ASSOC);

                    if (empty($ifExistHeading)) {
                        $this->db->query('INSERT INTO headings(url_id,heading) values("' . $url_id . '", "' . $heading . '")');
                    }

                }
            }

            if (!empty($titles[1])) {

                foreach ($titles[1] as $title) {

                    $ifExistTitle = $this->db->query('SELECT * FROM titles WHERE url_id = "' . $url_id . '" and title = "' . $title . '"')->fetch_all(MYSQLI_ASSOC);

                    if (empty($ifExistTitle)) {
                        $this->db->query('INSERT INTO titles(url_id,title) values("' . $url_id . '", "' . $title . '")');
                    }

                }
            }

            if (!empty($links[1])) {

                foreach ($links[1] as $link) {

                    if (substr($link, 0, 1) != '#' && strlen($link) != 1) {
                        $ifExistTitle = $this->db->query('SELECT * FROM inner_links WHERE url_id = "' . $url_id . '" and inner_link = "' . $link . '"')->fetch_all(MYSQLI_ASSOC);

                        if (empty($ifExistTitle)) {
                            $this->db->query('INSERT INTO inner_links(url_id,inner_link) values("' . $url_id . '", "' . $link . '")');
                        }
                    }

                }
            }

        }

        header('Location:index.php');
    }

    function search(){
        $search_val = $_POST['search'];

        if (strlen($search_val) == 0) {
            $_SESSION['error'] = 'Please add what to search!';
            header('Location:index.php');
        } else {
            $titles = $this->db->query("SELECT * FROM titles")->fetch_all(MYSQLI_ASSOC);
            $headings = $this->db->query("SELECT * FROM headings")->fetch_all(MYSQLI_ASSOC);
            $contents = $this->db->query("SELECT * FROM urls")->fetch_all(MYSQLI_ASSOC);
            $arr = array();

            if (!empty($titles)) {
                foreach ($titles as $title) {
                    if (strpos(strtolower($title['title']), strtolower($search_val)) !== false) {
                        $arr[] = $title['url_id'];
                    }
                }
            }
            if (!empty($headings)) {
                foreach ($headings as $heading) {
                    if (strpos(strtolower($heading['heading']), strtolower($search_val)) !== false) {
                        $arr[] = $heading['url_id'];
                    }
                }
            }
            if (!empty($contents)) {
                foreach ($contents as $content) {
                    if (strpos(strtolower($content['content']), strtolower($search_val)) !== false) {
                        $arr[] = $content['id'];
                    }
                }
            }
            $arr = array_unique($arr);

            if (empty($arr)) {
                $_SESSION['msg'] = 'No match';
            } else {
                $urls = $this->db->query("SELECT url FROM urls WHERE id in (" . implode(',', $arr) . ")")->fetch_all(MYSQLI_ASSOC);
                
                $_SESSION['msg'] = 'Your results!';
                $_SESSION['res_values'] = $urls;
                $_SESSION['search_val'] = $search_val;

            }
        }

        header('Location:index.php');
    }
}

$bot = new Bot();
?>